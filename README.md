# Bot-Facebook-RAC

_El proyecto va dirigido a los potenciales clientes, para mostrar proyectos, sucursales, y comunicarse con un asesor._



## Vista del Proyecto:




![admin](https://github.com/LauraSarita/Bot-Facebook-RAC/blob/master/img/img1.png?raw=true)

## Vista de Sucursales

![admin](https://github.com/LauraSarita/Bot-Facebook-RAC/blob/master/img/rac3.jpg?raw=true)


## Vista de Apartamentos 

![admin](https://github.com/LauraSarita/Bot-Facebook-RAC/blob/master/img/apt.jpg?raw=true)


## Construido con


* [Facebook Developers](https://developers.facebook.com) 



## Autores


* **Laura Sarita Gallego Martinez** - [GitHub](https://github.com/LauraSarita)

